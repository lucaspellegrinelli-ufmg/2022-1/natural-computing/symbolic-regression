import json
import numpy as np
import matplotlib.pyplot as plt
plt.style.use("_plotstyling.mplstyle")

with open("output/experiment-1-synth2.json", "r") as jf:
    expr_a_content = json.load(jf)

with open("output/experiment-2-synth2.json", "r") as jf:
    expr_b_content = json.load(jf)

a_train = []
b_train = []
a_test = []
b_test = []

for i, c in enumerate(expr_a_content):
    for _ in range(5):
        a_train.append(c["train_repeated"])
        a_test.append(c["test_repeated"])

for i, c in enumerate(expr_b_content):
    for _ in range(5):
        b_train.append(c["train_repeated"])
        b_test.append(c["test_repeated"])

start = 0
xs = list(range(start, len(a_train[0])))
a_train = np.array(a_train)[:, start:]
b_train = np.array(b_train)[:, start:]
a_test = np.array(a_test)[:, start:]
b_test = np.array(b_test)[:, start:]

train_style = {"linestyle":"--", "linewidth": 2, "markeredgewidth": 2, "marker": ""}

plt.title(f"Porcentagem de indivíduos iguais x Geração - synth2.csv")
plt.xlabel("Geração")
plt.ylabel("Porcentagem de indivíduos iguais")
plt.errorbar(xs, a_test.mean(axis=0), a_test.std(axis=0), linestyle="-", label="(Teste) % Crossover alto")
plt.errorbar(xs, b_test.mean(axis=0), b_test.std(axis=0), linestyle="-", label="(Teste) % Crossover baixo")
plt.plot(xs, a_train.mean(axis=0), **train_style, label="(Treino) % Crossover alto")
plt.plot(xs, b_train.mean(axis=0), **train_style, label="(Treino) % Crossover baixo")
plt.legend()
plt.tight_layout()
plt.savefig("plot.png")