import pandas as pd
from symbolicregressor import SymbolicRegressor

train_df = pd.read_csv(f"datasets/synth2/synth2-train.csv", header=None)

train_X = train_df.iloc[:, :-1].to_numpy()
train_y_true = train_df.iloc[:, -1].to_numpy()

config = {
    "n_individuals": 100,
    "individual_sizes": 6,
    "n_variables": train_X.shape[1],
    "crossover_prob": 0.9,
    "mutation_prob": 0.05,
    "tournament_size": 2,
    "elitism": True,
    "elitistic_operator": False
}

def callback(iteration, individuals):
    print(f" > Iteration #{iteration + 1}/10")

regressor = SymbolicRegressor(**config)
regressor.initialize_individuals()
best_indv, best_indv_fit = regressor.fit(train_X, train_y_true, iterations=10, callback=callback)