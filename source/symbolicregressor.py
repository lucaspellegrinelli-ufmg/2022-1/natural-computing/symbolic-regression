import random
import copy

from symbolicindividual import SymbolicIndividual

class SymbolicRegressor:
    def __init__(self, n_individuals, individual_sizes, n_variables, crossover_prob, mutation_prob, elitistic_operator, elitism=True, tournament_size=4):
        self.n_individuals = n_individuals
        self.individual_sizes = individual_sizes
        self.n_variables = n_variables
        self.crossover_prob = crossover_prob
        self.mutation_prob = mutation_prob
        self.elitism = elitism
        self.elitistic_operator = elitistic_operator
        self.tournament_size = tournament_size
        self.individuals = []
        self.best_individual = (None, float("inf"))

    def initialize_individuals(self):
        self.individuals = [
            SymbolicIndividual(
                size=self.individual_sizes,
                n_variables=self.n_variables
            ) for _ in range(self.n_individuals)
        ]

    def fit(self, X, y_true, iterations, callback=None):
        for i in range(iterations):
            self.iteration(X, y_true)
            if callback is not None:
                callback(i, self.individuals)

        fitness_list = self.calculate_fitness_list(X, y_true)
        elitism_indv, elitism_fitness = self.elitism_selection(fitness_list)
        self.report_best_individual_candidate(elitism_indv, elitism_fitness)
        return self.best_individual

    def iteration(self, X, y_true):
        fitness_list = self.calculate_fitness_list(X, y_true)
        new_population = []

        elitism_indv, elitism_fitness = self.elitism_selection(fitness_list)
        self.report_best_individual_candidate(elitism_indv, elitism_fitness)
        if self.elitism:
            new_population.append(elitism_indv)
        
        while len(new_population) < self.n_individuals:
            tournament_indv_a, a_fitness = self.tournament_selection(fitness_list, self.tournament_size)
            tournament_indv_b, b_fitness = self.tournament_selection(fitness_list, self.tournament_size)

            parent_a = copy.copy(tournament_indv_a)
            parent_b = copy.copy(tournament_indv_b)

            if random.uniform(0, 1) < self.crossover_prob:
                new_individual = self.crossover(parent_a, parent_b)
            else:
                new_individual = copy.deepcopy(parent_a if random.uniform(0, 1) < 0.5 else parent_b)

            new_indv_fitness = new_individual.fitness(X, y_true)
            if self.elitistic_operator:
                if new_indv_fitness < a_fitness and new_indv_fitness < b_fitness:
                    new_population.append(new_individual)
                else:
                    new_population.append(copy.deepcopy(parent_a if a_fitness < b_fitness else parent_b))
            else:
                new_population.append(new_individual)

        for i, new_indv in enumerate(new_population):
            if random.uniform(0, 1) < self.mutation_prob:
                new_population[i] = self.mutate(new_indv)

        self.individuals = new_population

    def tournament_selection(self, fitness_list, K):
        players = list(zip(self.individuals, fitness_list))
        group = random.sample(players, K)
        champion, fitness = min(group, key=lambda p: p[1])
        return champion, fitness

    def elitism_selection(self, fitness_list):
        players = list(zip(self.individuals, fitness_list))
        selected, fitness = min(players, key=lambda p: p[1])
        return selected, fitness

    def report_best_individual_candidate(self, individual, fitness):
        if fitness < self.best_individual[1]:
            self.best_individual = (individual, fitness)

    def mutate(self, a):
        new_individual = copy.deepcopy(a)
        r_ind = random.randint(1, 2 ** self.individual_sizes - 1)
        new_node, depth, _ = new_individual.symbol_tree.find_index(self.individual_sizes, r_ind)
        new_node.randomize_tree(self.individual_sizes - depth, self.n_variables)
        return new_individual

    def crossover(self, a, b):
        mid = 2 ** self.individual_sizes / 2
        r_ind = mid
        while r_ind == mid:
            r_ind = random.randint(1, 2 ** self.individual_sizes - 1)

        use_a_as_base = random.uniform(0, 1) < 0.5
        new_individual = copy.deepcopy(a if use_a_as_base else b)

        a_node, _, last_left = a.symbol_tree.find_index(self.individual_sizes, r_ind)
        b_node, _, last_left = b.symbol_tree.find_index(self.individual_sizes, r_ind)
        new_node, _, last_left = new_individual.symbol_tree.find_index(self.individual_sizes, r_ind)

        a_copy = copy.deepcopy(a_node)
        b_copy = copy.deepcopy(b_node)

        new_genes = a_copy if not use_a_as_base else b_copy
        if not last_left:
            new_node.parent.left_tree.update(new_genes)
        else:
            new_node.parent.right_tree.update(new_genes)
        
        return new_individual

    def calculate_fitness_list(self, X, y_true):
        return [ indv.fitness(X, y_true) for indv in self.individuals ]