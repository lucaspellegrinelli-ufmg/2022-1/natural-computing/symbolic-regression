import json
import numpy as np
import pandas as pd
from symbolicregressor import SymbolicRegressor

def experiments_to_dataset(dataset):
    train_df = pd.read_csv(f"datasets/{dataset}/{dataset}-train.csv", header=None)
    test_df = pd.read_csv(f"datasets/{dataset}/{dataset}-test.csv", header=None)

    train_X = train_df.iloc[:, :-1].to_numpy()
    train_y_true = train_df.iloc[:, -1].to_numpy()

    test_X = test_df.iloc[:, :-1].to_numpy()
    test_y_true = test_df.iloc[:, -1].to_numpy()

    def run_experiment(config, filename, iterations, repetitions):
        print("Running experiment", filename)

        for _ in range(repetitions):
            train_worst = []
            train_best = []
            train_mean = []
            train_median = []
            train_repeated = []
            
            test_worst = []
            test_best = []
            test_mean = []
            test_median = []
            test_repeated = []

            def callback(iteration, individuals):
                train_fitness_list = [ indv.fitness(train_X, train_y_true) for indv in individuals ]
                test_fitness_list = [ indv.fitness(test_X, test_y_true) for indv in individuals ]

                train_worst.append(max(train_fitness_list))
                train_best.append(min(train_fitness_list))
                train_mean.append(np.mean(train_fitness_list))
                train_median.append(np.median(train_fitness_list))
                train_repeated.append((len(train_fitness_list) - len(np.unique(train_fitness_list))) / len(train_fitness_list))

                test_worst.append(max(test_fitness_list))
                test_best.append(min(test_fitness_list))
                test_mean.append(np.mean(test_fitness_list))
                test_median.append(np.median(test_fitness_list))
                test_repeated.append((len(test_fitness_list) - len(np.unique(test_fitness_list))) / len(test_fitness_list))

                print(f" > Iteration #{iteration + 1}/{iterations}")

            regressor = SymbolicRegressor(**config)
            regressor.initialize_individuals()
            best_indv, best_indv_fit = regressor.fit(train_X, train_y_true, iterations=iterations, callback=callback)

            experiment_results = {
                "best_individual": best_indv.symbol_tree.tree_repr(),
                "best_fitness": best_indv_fit,
                "train_worst": train_worst,
                "train_best": train_best,
                "train_mean": train_mean,
                "train_median": train_median,
                "train_repeated": train_repeated,
                "test_worst": test_worst,
                "test_best": test_best,
                "test_mean": test_mean,
                "test_median": test_median,
                "test_repeated": test_repeated
            }

            json_entry = dict(list(config.items()) + list(experiment_results.items()))

            try:
                with open(filename, "r") as jf: contents = json.load(jf)
            except:
                contents = []
            contents.append(json_entry)
            with open(filename, "w") as jf: json.dump(contents, jf)

    # config = {
    #     "n_individuals": 100,
    #     "individual_sizes": 7,
    #     "n_variables": test_X.shape[1],
    #     "crossover_prob": 0.9,
    #     "mutation_prob": 0.05,
    #     "tournament_size": 2,
    #     "elitism": True,
    #     "elitistic_operator": False
    # }

    # run_experiment(config, f"output/experiment-1-{dataset}.json", iterations=25, repetitions=25)

experiments_to_dataset("synth1")
experiments_to_dataset("synth2")
experiments_to_dataset("concrete")