import numpy as np

from symbolicrepresentation import *

class SymbolicIndividual:
    def __init__(self, size, n_variables, initialize_random=True):
        self.size = size
        self.n_variables = n_variables
        self.symbol_tree = Symbol.random_non_terminal(parent=None)

        if initialize_random:
            self.symbol_tree.randomize_tree(self.size, self.n_variables)

    def predict(self, X):
        return np.array([ self.symbol_tree.evaluate(tx) for tx in X ])

    def fitness(self, X, y_true):
        y_pred = self.predict(X)
        nominator = np.sum((y_true - y_pred) ** 2)
        denom = np.sum((y_true - np.mean(y_true)) ** 2)
        return np.sqrt(nominator / denom)