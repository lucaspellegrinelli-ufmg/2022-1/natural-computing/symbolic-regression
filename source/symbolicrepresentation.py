import math
import random

class Symbol:
    ADD = 0
    SUB = 1
    MULT = 2
    DIV = 3
    CONSTANT = 4
    VARIABLE = 5

    NON_TERMINALS = [ADD, SUB, MULT, DIV]
    TERMINALS = [CONSTANT, VARIABLE]
    
    MIN_CONSTANT = 0
    MAX_CONSTANT = 9

    def __init__(self, type, parent, value=None, left_tree=None, right_tree=None):
        self.type = type
        self.parent = parent
        self.left_tree = left_tree
        self.right_tree = right_tree
        self.value = value

    def is_leaf(self):
        return self.type == Symbol.CONSTANT or self.type == Symbol.VARIABLE

    @staticmethod
    def random_non_terminal(parent):
        return Symbol(type=random.choice(Symbol.NON_TERMINALS), parent=parent)

    @staticmethod
    def random_terminal(parent, n_variables):
        if random.uniform(0, 1) > 0.5:
            return Symbol(
                type=Symbol.CONSTANT,
                parent=parent,
                value=random.randint(Symbol.MIN_CONSTANT, Symbol.MAX_CONSTANT)
            )
        else:
            return Symbol(
                type=Symbol.VARIABLE,
                parent=parent,
                value=random.randint(0, n_variables - 1)
            )

    def randomize_tree(self, tree_depth, n_variables):
        if self.is_leaf():
            self = Symbol.random_terminal(parent=self.parent, n_variables=n_variables)
        else:
            open_list = [self]
            depth = 0
            while depth < tree_depth:
                depth += 1
                new_open_list = []
                for node in open_list:
                    if depth < tree_depth - 1:
                        node.left_tree = Symbol.random_non_terminal(parent=node)
                        node.right_tree = Symbol.random_non_terminal(parent=node)
                        new_open_list += [node.left_tree, node.right_tree]
                    else:
                        node.left_tree = Symbol.random_terminal(parent=node, n_variables=n_variables)
                        node.right_tree = Symbol.random_terminal(parent=node, n_variables=n_variables)
                open_list = new_open_list

    def evaluate(self, X):
        if self.type == Symbol.CONSTANT:
            return self.value
        elif self.type == Symbol.VARIABLE:
            return X[self.value]
        elif self.type == Symbol.ADD:
            return self.left_tree.evaluate(X) + self.right_tree.evaluate(X)
        elif self.type == Symbol.SUB:
            return self.left_tree.evaluate(X) - self.right_tree.evaluate(X)
        elif self.type == Symbol.MULT:
            return self.left_tree.evaluate(X) * self.right_tree.evaluate(X)
        elif self.type == Symbol.DIV:
            denom = self.right_tree.evaluate(X)
            return self.left_tree.evaluate(X) / denom if denom != 0 else 1

    def update(self, role_model):
        self.left_tree = role_model.left_tree
        self.right_tree = role_model.right_tree
        self.type = role_model.type
        self.value = role_model.value

    def find_index(self, depth, index, verbose=False):
        # Busca binária para encontrar o nó escolhido
        idx = 2 ** depth / 2
        step = idx // 2
        curr_node = self
        depth = 0
        last_left = True
        if verbose: print("idx, step", idx, step)
        while idx != index:
            depth += 1
            last_left = idx >= index
            curr_node = curr_node.left_tree if idx < index else curr_node.right_tree
            idx += math.floor((1 if idx < index else -1) * step)
            step /= 2
            if verbose: print("idx, step", idx, step)

        if verbose: print("[]", curr_node)
        return curr_node, depth, last_left

    def __repr__(self):
        if self.type == Symbol.CONSTANT:
            return f"{self.value}"
        elif self.type == Symbol.VARIABLE:
            return chr(ord("a") + self.value)
        elif self.type == Symbol.ADD:
            return "+"
        elif self.type == Symbol.SUB:
            return "-"
        elif self.type == Symbol.MULT:
            return "*"
        elif self.type == Symbol.DIV:
            return "/"

    def tree_repr(self):
        def preorder(node, depth):
            out = str(node)
            if not self.is_leaf():
                if node.left_tree is not None:
                    out += " " + str(preorder(node.left_tree, depth + 1))
                if node.right_tree is not None:
                    out += " " + str(preorder(node.right_tree, depth + 1))
            return out

        return preorder(self, depth=0)